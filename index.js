function Pokemon (name, level){

	// Properties
	this.name = name;
	this.lvl = level;
	this.health = 5* level;
	this.atk = level;

	// Methods
	this.scratch = function (target){
		let result = target.health - this.atk;
		target.health = result;		
		console.log(`${this.name} scratched ${target.name}`)
		console.log(`${target.name} health is now reduced to (${result})`);
		if (target.health < 10){
			target.faint()
		};
	};
	this.tackle = function (target){
		let result = target.health - this.atk;
		target.health = result;		
		console.log(`${this.name} tackled ${target.name}`)
		console.log(`${target.name} health is now reduced to (${result})`);
		if (target.health < 10){
			target.faint()
		
		};
	};

	this.faint = function(){
		console.log(`${this.name} fainted`)
	};
};
	

let bulbasaur = new Pokemon("bulbasaur", 10);
let rattata = new Pokemon("rattata", 10);

console.log(bulbasaur);
console.log(rattata);

bulbasaur.scratch(rattata);
rattata.tackle	(bulbasaur);
bulbasaur.scratch(rattata);
rattata.tackle	(bulbasaur);
bulbasaur.scratch(rattata);
rattata.tackle	(bulbasaur);
bulbasaur.scratch(rattata);
rattata.tackle	(bulbasaur);
bulbasaur.scratch(rattata);





